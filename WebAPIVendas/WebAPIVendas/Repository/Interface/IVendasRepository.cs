﻿using Microsoft.EntityFrameworkCore.Update.Internal;
using WebAPIVendas.Models;

namespace WebAPIVendas.Repository.Interface
{
    public interface IVendasRepository
    {
        public Vendas GetSaleById(int id);
        void UpdateStatusSale(int idVenda, string statusVenda);
        void PutSales(Vendas vendas);

    }
}
