﻿using Microsoft.EntityFrameworkCore;
using WebAPIVendas.Models;
using WebAPIVendas.Repository.Interface;

namespace WebAPIVendas.Repository
{
    public class VendasRepository : IVendasRepository
    {
        public Vendas GetSaleById(int id)
        {
            var contextOptions = new DbContextOptionsBuilder<ApiVendasContext>().UseInMemoryDatabase("InMemoryDb").Options;
            ApiVendasContext context = new ApiVendasContext(contextOptions);

            var vendascontext = context.vendas.Where(s => s.Id == id).FirstOrDefault();
            Vendas vendas = new Vendas();

            if (vendascontext != null)
            {
                var vendedores = context.vendors.Where(v => v.Id == vendascontext.IdVendedor).FirstOrDefault();
                var itemsvendascontext = context.itemsVendas.Where(i => i.IdVendas == id).FirstOrDefault();

                vendas.Id = vendascontext.Id;
                vendas.DataVenda = vendascontext.DataVenda;
                vendas.StatusVenda = vendascontext.StatusVenda;
                vendas.IdVendedor = vendascontext.IdVendedor;
                vendas.vendedor = vendedores;
                vendas.itensVendas = itemsvendascontext;
            }

            return vendas;
        }

        public void UpdateStatusSale(int idVenda, string statusVenda)
        {
            var contextOptions = new DbContextOptionsBuilder<ApiVendasContext>().UseInMemoryDatabase("InMemoryDb").Options;
            ApiVendasContext context = new ApiVendasContext(contextOptions);

            var vendascontext = context.vendas.Where(s => s.Id == idVenda).FirstOrDefault();
            if (vendascontext != null)
            {
                vendascontext.StatusVenda = statusVenda;
                context.SaveChanges();
            };            
        }

        public void PutSales(Vendas vendas)
        {
            var contextOptions = new DbContextOptionsBuilder<ApiVendasContext>().UseInMemoryDatabase("InMemoryDb").Options;
            ApiVendasContext context = new ApiVendasContext(contextOptions);

            context.vendors.Add(vendas.vendedor);            
            context.vendas.Add(vendas);
            context.itemsVendas.Add(vendas.itensVendas);
            context.SaveChanges();
        }
    }
}
