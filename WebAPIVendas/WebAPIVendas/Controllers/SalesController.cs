using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using WebAPIVendas.Models;
using WebAPIVendas.Repository.Interface;

namespace WebAPIVendas.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SalesController : ControllerBase
    {
        private readonly ILogger<SalesController> _logger;
        private readonly IVendasRepository _vendasRepository;

        public SalesController(ILogger<SalesController> logger,
                               IVendasRepository vendasRepository)
        {
            _logger = logger;
            _vendasRepository = vendasRepository;
        }

        /// <summary>
        /// Get Sale
        /// </summary>
        /// <remarks>Get Sale by Id</remarks>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet(Name = "GetSales")]
        public Vendas GetSales(int Id)
        {
            var vendas = _vendasRepository.GetSaleById(Id);
            return vendas;
        }

        /// <summary>
        /// Insert Sale
        /// </summary>
        /// <remarks>Insert Sale</remarks>
        /// <returns></returns>
        [HttpPut(Name = "PutSales")]
        public int PutSale(Vendas vendas)
        {
            try
            {
                _vendasRepository.PutSales(vendas);
                return StatusCodes.Status200OK;
            }
            catch (Exception ex)
            {
                throw new($"Erro ao inserir vendas. StackTrace: {ex.StackTrace}");
            }
        }

        /// <summary>
        /// Update Status Sales
        /// </summary>
        /// <remarks>Update Status Sales</remarks>
        /// <param name="idVenda"></param>
        /// <param name="statusVenda"></param>
        /// <returns></returns>
        [HttpPost(Name = "UpdateSales")]
        public Vendas UpdateSales(int idVenda, string statusVenda)
        {
            var vendas = new Vendas();
            try
            {
                var vendasStatus = _vendasRepository.GetSaleById(idVenda);
                bool mudarStatus = false;

                if (vendasStatus != null)
                {
                    if (vendasStatus.StatusVenda == "Aguardando pagamento" && statusVenda == "Pagamento Aprovado" ||
                        vendasStatus.StatusVenda == "Aguardando pagamento" && statusVenda == "Cancelada" ||
                        vendasStatus.StatusVenda == "Pagamento Aprovado" && statusVenda == "Enviado para Transportadora" ||
                        vendasStatus.StatusVenda == "Pagamento Aprovado" && statusVenda == "Cancelada" ||
                        vendasStatus.StatusVenda == "Enviado para Transportadora" && statusVenda == "Entregue")
                        mudarStatus = true;
                    else mudarStatus = false;

                    if (mudarStatus)
                    {
                        _vendasRepository.UpdateStatusSale(idVenda, statusVenda);
                        vendas = _vendasRepository.GetSaleById(idVenda);
                    }
                    else throw new ValidationException($"Status atual {vendasStatus.StatusVenda} n�o permite ser atualizado para {statusVenda}");
                }                
            }
            catch (ValidationException) 
            {
                throw;
            }
            return vendas;
        }

    }
}
