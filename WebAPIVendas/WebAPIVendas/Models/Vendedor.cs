﻿namespace WebAPIVendas.Models
{
    public class Vendedor
    {
        public int Id { get; set; }
        public required string CPF { get; set; }
        public required string Nome { get; set; }
        public string? Email { get; set;}
        public string? Telefone { get; set; }

    }
}
