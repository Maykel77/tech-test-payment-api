﻿namespace WebAPIVendas.Models
{
    public class ItensVendas
    {
        public int Id { get; set; }
        public required int IdVendas { get; set; }
        public required string Descricao { get; set; }
    }
}
