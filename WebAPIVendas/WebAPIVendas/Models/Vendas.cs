﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

namespace WebAPIVendas.Models
{
    public class Vendas
    {
        public int Id { get; set; }
        public DateTime DataVenda { get; set; }
        public int IdVendedor { get; set; }
        public Vendedor vendedor { get; set; }
        public string StatusVenda { get; set; }
        public ItensVendas itensVendas { get; set; }
    }

    public class ApiVendasContext : DbContext
    {
        public ApiVendasContext(DbContextOptions<ApiVendasContext> options)
          : base(options)
        { }

        public DbSet<Vendas> vendas { get; set; }
        public DbSet<Vendedor> vendors { get; set; }
        public DbSet<ItensVendas> itemsVendas { get; set; }

    }
}
